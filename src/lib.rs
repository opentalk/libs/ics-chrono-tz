// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: MIT OR Apache-2.0

#![allow(non_camel_case_types, clippy::unreadable_literal)]

include!(concat!(env!("OUT_DIR"), "/lib.rs"));
